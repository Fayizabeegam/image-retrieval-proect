import datetime
import random
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from flask import Flask,render_template,request,jsonify,redirect,url_for

from flask.globals import session
from DBConnection import conn
from dbconnection2 import Db
app = Flask(__name__)
app.secret_key="hai"
c=conn()
static_path="D://mes_crime_scin//riss//work//crm//static//"
static_project="D://mes_crime_scin//riss//work//crm//"
static_path2="D://mes_crime_scin//object matching//"



@app.route('/')
def login():
    return render_template('login.html')
@app.route('/login_post',methods=['post'])
def login_post():
    c=conn()
    uname=request.form['name']
    passwd=request.form['pwd']
    s="select * from login where user_name='"+uname+"' and password='"+passwd+"'"
    res=c.selectone(s)
    if res==None:
        return render_template('login.html')
    else:
        type=res[2]
        if type=="admin":
            session['lg']='y'
            return  render_template('/Admin/home.html')

        elif type=="staff":
            session['lg'] = 'y'
            s = "select oid from officer where email='"+uname+"'"
            r = c.selectone(s)
            session["conid22"] = r[0]
            session["oid"] = r[0]

            return render_template('/officer_ci/home.html')

        else:
            return render_template('login.html')
@app.route("/logout")
def logout():
    session['lg']='n'
    return render_template("login.html")



@app.route('/adm_hom')
def admin_home():
    return render_template("Adminhome.html")
@app.route('/si_hm')
def si_hm():
    return render_template("officer_si/home.html")
@app.route('/ci_hm')
def ci_hm():
    return render_template("officer_ci/home.html")



@app.route('/adm_hm')
def adm_hm():
    return render_template("Admin/home.html")

# admin : view crime
@app.route('/adm_crm_vw')
def adm_crm_vw():
    slc = "select * from crime"
    res = c.selectall(slc)
    return render_template('/admin/crime_view.html',data=res)
@app.route('/adm_viw_crime_sen/<id>')           ##  view crime scenes
def adm_crm_desc(id):
    print(id)
    qr="select scene_file,cid from crime_scene where cr_id='"+str(id)+"'"
    res=c.selectall(qr)
    print(res)
    return render_template("Admin/crime_scene_view.html",data=res)
@app.route("/adm_view_scen_objs/<sc_id>")       ##   view scene objects
def adm_view_scen_objs(sc_id):
    qry="select name,details,path from scene_objects where scn_id='"+sc_id+"'"
    res=c.selectall(qry)
    return render_template("Admin/adm_view_scene_objects.html",data=res)

###   admin : police station management
@app.route('/adm_police_stn_regi')
def adm_police_stn_regi():
    return render_template("Admin/police_station_regi.html")

@app.route('/police_stn_regi',methods=['POST'])
def police_stn_regi():
    photos=request.files["photos"]
    name=request.form["name"]
    place=request.form["place"]
    post=request.form["post"]
    pincode=request.form["pincode"]
    district=request.form["district"]
    email=request.form["email"]
    phone=request.form["phone"]
    photos.save(static_path+"policestation\\"+photos.filename)
    path='/static/policestation/'+photos.filename

    ins="insert into police_station (name,place,post,pincode,district,email,phone,photos)values('"+name+"','"+place+"','"+post+"','"+str(pincode)+"','"+district+"','"+email+"','"+phone+"','"+path+"')"
    res=c.nonreturn(ins)
    if res is not None:
        return render_template("Admin/home.html")


@app.route('/adm_police_stn_vw')
def adm_police_stn_vw():
    slc="select * from police_station"
    res=c.selectall(slc)
    return render_template("Admin/police_station_view.html",data=res)

@app.route('/adm_police_stn_dlt/<pid>')
def adm_police_stn_dlt(pid):
    qry="delete from police_station where pid='"+pid+"'"
    res=c.nonreturn(qry)
    return adm_police_stn_vw()

@app.route('/adm_police_stn_edit/<pid>')
def adm_police_stn_edit(pid):
    edt="select * from police_station where pid='"+str(pid)+"'"
    res=c.selectone(edt)
    qq="/static/policestation/a1.jpg"
    ss=str(res[8])
    mmss=ss.split("/")
    session["pic7"]=mmss[3]
    print(mmss[3])
    print("ov")

    print(edt)
    print(res)
    return render_template("Admin/police_station_edit.html",data=res)

@app.route('/adm_police_stn_btn',methods=['POST'])
def adm_police_stn_btn():
    id=request.form["pid"]
    name = request.form["name"]
    place = request.form["place"]
    post = request.form["post"]
    pincode = request.form["pincode"]
    district = request.form["district"]
    phone = request.form["phone"]
    if 'photos' in request.files:
        photos = request.files["photos"]
        if photos.filename=="":
            qry = "update police_station set name ='" + name + "',place='" + place + "',post='" + post + "',pincode ='" + pincode + "',district='" + district + "',phone ='" + phone + "' where pid ='" + id + "'"
        else:
            photos.save(static_path+"policestation\\" + photos.filename)
            path = '/static/policestation/' + photos.filename
            qry = "update police_station set name ='" + name + "',place='" + place + "',post='" + post + "',pincode ='" + pincode + "',district='" + district + "',phone ='" + phone + "',photos ='" + path + "' where pid ='" + id + "'"
    else:
        qry = "update police_station set name ='" + name + "',place='" + place + "',post='" + post + "',pincode ='" + pincode + "',district='" + district + "',phone ='" + phone + "' where pid ='" + id + "'"
    print(qry)
    c.nonreturn(qry)
    return adm_police_stn_vw()


##   admin : police officer management
@app.route('/adm_ofcr_add')
def adm_ofcr_add():
    qry="select pid,name from police_station"
    res=c.selectall(qry)
    print(res)
    return render_template("Admin/officer_add.html",data22=res)

@app.route('/ofcr_add',methods=['POST'])
def ofcr_add():
    print("m1")
    photos=request.files["im"]
    name=request.form["name"]
    police_station=request.form["police station"]
    # position='si'
    age=request.form["age"]
    qualification=request.form["qualification"]
    experience=request.form["experience"]
    house_name=request.form["house name"]
    place=request.form["place"]
    pincode=request.form["pincode"]
    email=request.form["email"]
    phone=request.form["phone"]
    pos=request.form["pos"]
    print("m23")
    path = 'static/officers/' + photos.filename
    photos.save(static_path+"officers\\" + photos.filename)

    ins="insert into officer(pid,name,position,age,qualification,experience,house_name,place,pincode,email,phone,photos)values('"+str(police_station)+"','"+name+"','"+pos+"','"+str(age)+"','"+qualification+"','"+experience+"','"+house_name+"','"+place+"','"+str(pincode)+"','"+email+"','"+phone+"','"+path+"')"
    print("ins")
    res=c.nonreturn(ins)
    pswd = str(random.randint(0000, 9999))
    ins2="insert into login (user_name,password,type)values('"+email+"','"+pswd+"','staff') "
    res2 = c.nonreturn(ins2)

    # import smtplib  # mail sending
    # s = smtplib.SMTP(host='smtp.gmail.com', port=587)
    # s.starttls()
    # s.login("smartfuddonation@gmail.com", "smart@789")
    # msg = MIMEMultipart()  # create a message.........."
    # message = "Message from OBJECT MATCHING"
    # msg['From'] = "smartfuddonation@gmail.com"
    # msg['To'] = email
    # msg['Subject'] = "Your Password for OBJECT MATCHING Website"
    # body = "Your Password is :- - " + str(pswd)
    # msg.attach(MIMEText(body, 'plain'))
    # s.send_message(msg)
    if res is not None:
        print("ooo")
        return render_template("Admin/home.html")

@app.route('/adm_ofcr_vw')
def adm_ofcr_vw():
    slc="select officer.*,police_station.name from officer inner join police_station on police_station.pid=officer.pid"
    res=c.selectall(slc)
    return render_template("Admin/officer_view.html",data=res)

@app.route('/adm_ofcr_dlt/<oid>')
def adm_ofcr_dlt(oid):
    dlt = "delete from officer where oid='" + str(oid) + "'"
    c.nonreturn(dlt)
    return adm_ofcr_vw()

@app.route('/adm_ofcr_edit/<oid>')
def adm_ofcr_edit(oid):
    qry="select officer.*,police_station.name from officer inner join police_station on police_station.pid=officer.pid where officer.oid='"+oid+"'"
    res = c.selectone(qry)
    qry1="select pid,name from police_station"
    res1=c.selectall(qry1)
    return render_template("Admin/officer_edit.html",data=res,data1=res1)

@app.route('/adm_ofcr_btn',methods=['POST'])
def adm_ofcr_btn():
    id=request.form["mm"]
    name = request.form["name"]
    police_station = request.form["ps"]
    age = request.form["age"]
    qualification = request.form["ql"]
    experience = request.form["exp"]
    house_name = request.form["hn"]
    place = request.form["place"]
    pincode = request.form["pin"]
    phone = request.form["phone"]
    pos = request.form["pos"]
    if 'im' in request.files:
        photos = request.files["im"]
        photos22 = request.files["im"]

        print("kjkk=",photos)
        print("kjk22k=", photos.filename)

        if photos.filename=="":
            qry = "update officer set position='"+pos+"',name='" + name + "',age='" + age + "',qualification='" + qualification + "',experience='" + experience + "',house_name='" + house_name + "',place='" + place + "',pincode='" + pincode + "',phone='" + phone + "' where oid = '" + str(id) + "'"
        else:
            print("mrr")
            photos.save(static_path+"officers\\" + photos.filename)


            path = 'static/officers/' + photos.filename
            qry = "update officer set position='"+pos+"',name='" + name + "',age='" + age + "',qualification='" + qualification + "',experience='" + experience + "',house_name='" + house_name + "',place='" + place + "',pincode='" + pincode + "',phone='" + phone + "',photos='" + path + "' where oid = '" + str(id) + "'"
    else:
        qry = "update officer set position='"+pos+"',name='" + name + "',age='" + age + "',qualification='" + qualification + "',experience='" + experience + "',house_name='" + house_name + "',place='" + place + "',pincode='" + pincode + "',phone='" + phone + "' where oid = '" + str(id) + "'"
    c.nonreturn(qry)
    return adm_ofcr_vw()


##  admin : training set management
@app.route('/adm_tng_st_add')
def adm_tng_st_add():
    return render_template("Admin/training_set_add.html")

@app.route('/tng_st_add',methods=['POST'])
def tng_st_add():
    image=request.files["image"]
    name=request.form["name"]
    det=request.form['txt_det']
    # image.save("D:\\riss\\work\\crm\\static\\training_set\\"+image.filename)
    image.save(static_path2+"training_set\\" + image.filename)
    path='/training_set/'+image.filename                            ################## path????????????
    ins="insert into training_set(name,details,file)values('"+name+"','"+det+"','"+path+"')"
    res=c.nonreturn(ins)
    if res is not None:
        return render_template("Admin/home.html")

@app.route('/adm_tng_st_vw')
def adm_tng_st_vw():
    slc="select * from training_set"
    res=c.selectall(slc)
    return render_template("Admin/training_set_view.html",data=res)

@app.route('/adm_tng_st_dlt/<tid>')
def adm_tng_st_dlt(tid):
    qry = "delete from training_set where tr_set_id='" + tid+ "'"
    res = c.nonreturn(qry)
    return adm_tng_st_vw()

@app.route('/adm_tng_st_edt/<tid>')
def adm_tng_st_edit(tid):
    edt="select * from training_set where tr_set_id='"+str(tid)+"'"
    res=c.selectone(edt)
    return render_template("Admin/training_set_edit.html",data=res)

@app.route('/adm_tng_st_btn',methods=['POST'])
def adm_tng_st_btn():
    id=request.form["mhh"]
    name=request.form["name"]
    det=request.form['txt_det']
    if 'image' in request.files:
        image = request.files["image"]
        if image.filename=="":
            qry = "update training_set set name='" + name + "',details='"+det+"' where tr_set_id='" + id + "'"
        else:
            image.save(static_path2+"training_set\\" + image.filename)
            # path = '/static/training_set/' + image.filename
            path='/training_set/'+image.filename
            qry = "update training_set set name='" + name + "',details='"+det+"',file='" + path + "' where tr_set_id='" + id + "'"
    else:
        qry = "update training_set set name='" + name + "',details='" + det + "' where tr_set_id='" + id + "'"
    c.nonreturn(qry)
    return adm_tng_st_vw()






# @app.route('/adm_ofcr_allo_alloct')
# def adm_ofcr_allo_alloct():
#     qry = "select pid,name from police_station"
#     qrys="select oid,name from officer"
#     res=c.selectall(qry)
#     resl=c.selectall(qrys)
#     print(qry)
#     print(qrys)
#     return render_template("Admin/officer_allocation_allocate.html",data=res,data33=resl)
#
# @app.route('/ofcr_allo_alloct',methods=['POST'])
# def ofcr_allo_alloct():
#     officer=request.form["officer"]
#     station=request.form["station"]
#     ins="insert into officer_allocation(oid,pid)values('"+officer+"','"+station+"')"
#     res=c.nonreturn(ins)
#     if res is not None:
#         return render_template("Admin/home.html")
#
# @app.route('/adm_ofcr_allo_vw')
# def adm_ofcr_allo_vw():
#     slc="select officer_allocation.of_alloc_id,police_station.name,officer.name,officer.photos from officer,officer_allocation,police_station where police_station.pid=officer_allocation.pid and officer_allocation.oid=officer.oid"
#     print(slc)
#     res=c.selectall(slc)
#     print(res)
#     return render_template("Admin/officer_allocation_view.html",data=res)
#
# @app.route('/adm_ofcr_allo_dlt/<of_alloc_id>')
# def adm_ofcr_allo_dlt(of_alloc_id):
#     qry="delete from officer_allocation where of_alloc_id='"+str(of_alloc_id)+"'"
#     print(qry)
#
#     res = c.nonreturn(qry)
#     return render_template("Admin/home.html")
#
# @app.route('/adm_ofcr_allo_edit/<of_alloc_id>')
# def ofcr_allo_edit(of_alloc_id):
#     qry = "select pid,name from police_station"
#     qrys = "select oid,name from officer"
#     res = c.selectall(qry)
#     res44 = c.selectall(qrys)
#
#     return render_template("Admin/officer_allocation_edit.html",data=res,data33=res44,id55=of_alloc_id)
#
# @app.route('/adm_ofcr_alloc_btn',methods=['POST'])
# def adm_ofcr_alloc_btn():
#     id=request.form["hh"]
#     name = request.form["officer"]
#     station = request.form["station"]
#
#     qry = "update officer_allocation set oid='"+name+"',pid='"+station+"' where of_alloc_id='"+id+"'"
#     print(qry)
#
#     res = c.nonreturn(qry)
#     print(res)
#     return render_template('admin/home.html')
#






##############   CI
### ci : view profile
@app.route("/ci_view_profile")
def ci_view_profile():
    qry="select officer.*,police_station.name from police_station inner join officer on officer.pid=police_station.pid where officer.oid='"+str(session['conid22'])+"'"
    res=c.selectone(qry)
    return render_template("officer_ci/view_profile.html",data=res)

### ci : victim management
@app.route('/ci_vict_add')
def ci_vict_add():
    qry = "select cid,crime_name from crime"
    res = c.selectall(qry)
    return render_template("officer_ci/victim_add.html", data=res)

@app.route('/vict_add',methods=['POST'])
def vict_add():
    image=request.files["image"]
    crime = request.form["crime"]
    name=request.form["name"]
    age=request.form["age"]
    gndr=request.form["gndr"]
    house_name=request.form["house_name"]
    place=request.form["place"]
    pincode=request.form["pincode"]
    district=request.form["district"]
    phone=request.form["phone"]
    blood_group=request.form["blood_group"]
    id_number=request.form["id_number"]
    image.save(static_path+"victim\\"+image.filename)
    path='/static/victim/'+image.filename
    ins="insert into victim(cid,name,age,sex,house_name,place,pincode,district,phone,blood_group,id_number,photos) values('"+crime+"','"+name+"','"+age+"','"+gndr+"','"+house_name+"','"+place+"','"+pincode+"','"+district+"','"+phone+"','"+blood_group+"','"+id_number+"','"+path+"')"
    c=conn()
    res=c.nonreturn(ins)
    if res is not None:
        return ci_hm()

@app.route('/ci_vict_vw')
def ci_vict_vw():
    slc="select victim.vid,victim.name,victim.photos,victim.age,victim.sex,crime.crime_name from crime inner join victim on victim.cid=crime.cid"
    c=conn()
    res=c.selectall(slc)
    return render_template("officer_ci/victim_view.html",data=res)

@app.route('/ci_vict_dlt/<vid>')
def ci_vict_dlt(vid):
    dlt="delete from victim where vid='"+vid+"'"
    c=conn()
    res=c.nonreturn(dlt)
    return ci_vict_vw()

@app.route('/ci_vict_edit/<vid>')
def ci_vict_edit(vid):
    edt="select victim.*,crime.crime_name from crime inner join victim on crime.cid=victim.cid where victim.vid='"+vid+"'"
    print(edt)
    res=c.selectone(edt)
    print("rr=",res)
    qry = "select cid,crime_name from crime"
    res1 = c.selectall(qry)
    print(res1)
    print("RRR    ", res)
    print("CCC    ",res1)
    return render_template("officer_ci/victim_edit.html",data=res,crm=res1)

@app.route('/ci_updt_vict',methods=['POST'])
def ci_updt_vict():
    vid=request.form['vid']
    crime = request.form["crime"]
    name=request.form["name"]
    age=request.form["age"]
    gndr=request.form["gndr"]
    house_name=request.form["house_name"]
    place=request.form["place"]
    pincode=request.form["pincode"]
    district=request.form["district"]
    phone=request.form["phone"]
    blood_group=request.form["blood_group"]
    id_number=request.form["id_number"]
    if 'image' in request.files:
        image = request.files["image"]
        if image.filename=="":
            qry = "update victim set cid='" + crime + "',name='" + name + "',age='" + age + "',sex='" + gndr + "',house_name='" + house_name + "',place='" + place + "',pincode='" + pincode + "',district='" + district + "',phone='" + phone + "',blood_group='" + blood_group + "',id_number='" + id_number + "' where vid='" + vid + "'"
        else:
            image.save(static_path+"victim\\"+image.filename)
            path='/static/victim/'+image.filename
            qry="update victim set cid='"+crime+"',name='"+name+"',age='"+age+"',sex='"+gndr+"',house_name='"+house_name+"',place='"+place+"',pincode='"+pincode+"',district='"+district+"',phone='"+phone+"',blood_group='"+blood_group+"',id_number='"+id_number+"',photos='"+path+"' where vid='"+vid+"'"
    else:
        qry = "update victim set cid='" + crime + "',name='" + name + "',age='" + age + "',sex='" + gndr + "',house_name='" + house_name + "',place='" + place + "',pincode='" + pincode + "',district='" + district + "',phone='" + phone + "',blood_group='" + blood_group + "',id_number='" + id_number + "',photos='" + path + "' where vid='" + vid + "'"
    res=c.nonreturn(qry)
    if res is not None:
        return ci_hm()













@app.route('/ci_crm_add')
def ci_crm_add():
    qry = "	select of_alloc_id,oid from officer_allocation"
    print("qqqqqqq")
    c=conn()
    res =c.selectall(qry)
    print(qry)
    return render_template("officer_ci/crime_add.html",data44=res)

@app.route('/crm_add', methods=['POST'])
def crm_add():
    officer=request.form["officer"]
    name=request.form["name"]
    date=request.form["date"]
    place=request.form["place"]
    time=request.form["time"]
    type=request.form["type"]
    c = conn()
    ins = "insert into crime(of_alloc_id,name,date,place,time,type)values('"+str(officer)+"','"+name+"','"+date+"','"+place+"','"+time+"','"+type+"')"
    res=c.nonreturn(ins)
    print("nnnnnnnnnnnn")
    if res is not None:
        return render_template("officer_ci/home.html")

@app.route('/ci_crm_vw')
def ci_crm_vw():
    slc="select * from crime where cid='cid'"
    print("mmmmmmmmmmmm")
    c=conn()
    res=c.selectall(slc)
    print(res)
    return render_template("officer_ci/crime_view.html",data=res)

@app.route('/ci_crm_dlt/<cid>')
def ci_crm_dlt(cid):
    dlt="delete from crime where cid = 'cid'"
    c=conn()
    res=c.nonreturn(dlt)
    print(res)
    return ci_crm_vw()

@app.route('/ci_crm_edit')
def ci_crm_edit():
    return render_template("officer_ci/crime_edit.html")





@app.route('/ci_crm_scene_add')
def ci_crm_scene_add():
    qry="select cid,name from crime"
    c=conn()
    res=c.selectall(qry)
    print(res)
    return render_template("officer_ci/crime_scene_add.html",data=res)

@app.route('/crm_scene_add',methods=['POST'])
def crm_scene_add():
    image=request.files["image"]
    crime=request.form["crime"]
    date=request.form["dates"]
    image.save(static_path+"crime_scene\\"+image.filename)
    path='/static/'+image.filename
    c=conn()
    ins="insert into crime_scene(cid,date,path)values('"+str(crime)+"','"+date+"','"+path+"')"
    print("L")
    r = c.nonreturn(ins)
    if r is not None:
        return render_template("officer_ci/home.html")

@app.route('/ci_crm_scene_vw')
def ci_crm_scene_vw():
    slc="select * from crime_scene"
    c=conn()
    res=c.selectall(slc)
    return render_template("officer_ci/crime_scene_view.html",data11=res)

@app.route('/ci_crm_scene_dlt/<crid>')
def ci_crm_scene_dlt(crid):
    dlt="delete from crime_scene where crid = 'crid'"
    c=conn()
    res=c.nonreturn(dlt)
    return ci_crm_scene_vw()

@app.route('/ci_crm_scene_edit')
def ci_crm_scene_edit():
    return render_template("officer_ci/crime_scene_edit.html")









@app.route('/si_crm_desc')
def si_crm_desc():
    return render_template("officer_si/crime_description.html")


















@app.route("/a")
def a():
    return render_template("Admin/adm_temp.html")


##########


@app.route('/complaint_view')
def complaint_view():
    qry="select complaint.*,officer.name,officer.email from officer inner join complaint on officer.oid=complaint.pid and complaint.reply='pending'"
    db=conn()
    res=db.selectall(qry)

    return render_template("Admin/complaint_view.html",data=res)

@app.route('/reply/<jj>')
def reply(jj):
    session["id"]=jj
    qq="SELECT * FROM `complaint` WHERE `complaint_id`='"+jj+"'"
    ss=conn()
    rr=ss.selectone(qq)
    return render_template("Admin/Admin_complaint_reply.html",data=rr)


@app.route('/admin_complaint_reply_post',methods=['POST'])
def admin_complaint_reply_post():
    print("j")
    reply=request.form['textarea']
    print(reply)
    id =session['id']
    print("cid=",id)


    db=conn()
    qry="update complaint set reply='"+reply+"' where complaint_id='"+str(id)+"'"
    print(qry)
    res=db.nonreturn(qry)

    return '''<script>alert("reply send");window.location="/complaint_view";</script>'''

@app.route('/add_notification')
def add_notification():
    return render_template("Admin/add_notification.html")

@app.route('/add_notification',methods=['POST'])
def add_notificationpost():
    subject=request.form['textarea']
    notification=request.form['textarea2']
    db=conn()
    qry="INSERT INTO `notification`(`date`,`subject`,`noti`)VALUES(CURDATE(),'"+subject+"','"+notification+"')"
    res=db.nonreturn(qry)

    return '''<script>alert("ok");window.location="/a";</script>'''
@app.route('/view_notification')
def view_notification():
    db=conn()
    qry=" SELECT * FROM `notification` "
    res=db.selectall(qry)

    return render_template("Admin/view_notification.html",data=res)

@app.route('/editnotification/<noti>')
def editnotification(noti):
    session['id']=noti
    qry="select * from notification where nid='"+str(noti)+"'"
    db=conn()
    res=db.selectone(qry)
    return render_template ("Admin/edit_notification.html",data=res)

@app.route('/editnotificationpost/',methods=['post'])
def editnotificationpost():
    noti=session['id']
    subject=request.form['textarea']
    notification=request.form['textarea2']
    db=conn()
    qry="UPDATE`notification` SET`subject`='"+subject+"',`noti`='"+notification+"' WHERE`nid`='"+noti+"'"
    res=db.nonreturn(qry)
    return view_notification()


@app.route('/deletenotification/<delnoti>')
def deletenotification(delnoti):
    qry="DELETE FROM notification WHERE nid='"+delnoti+"'"
    print(qry)
    c=conn()
    ss=c.nonreturn(qry)
    return view_notification()



@app.route('/ofr_cmt_add')
def ofr_cmt_add():

    return render_template("officer_ci/cmd_add.html")


@app.route('/ofr_cmt_add_pst',methods=['post'])
def ofr_cmt_addpost():
    print("jjj")
    cmt=request.form['textarea']
    sub = request.form['sub']

    lid=session["oid"]
    print("QQQQ")

    db=conn()
    qry="insert into complaint(subject,complaint,date,reply,pid)values('"+sub+"','"+cmt+"',curdate(),'pending','"+str(lid)+"')"
    print(qry)
    res=db.nonreturn(qry)
    return render_template("officer_ci/cmd_add.html")

@app.route('/ofr_view_cmt_reply')
def ofr_view_cmt_reply():
    db=conn()
    pid=session["oid"]
    qry=" select complaint,reply,date from complaint where pid='"+str(pid)+"'"
    res=db.selectall(qry)

    return render_template("officer_ci/view_cmt_reply.html",data=res)

####################
@app.route('/fff/<id>')
def fff(id):
    print("hhh")
    # return "ok"
    oid =id
    session["toid22"]=oid
    print(oid)

    return render_template("officer_ci/fur_chat.html",toid=oid)

@app.route("/chat_usr_chk",methods=['post'])        # refresh messages chatlist
def chat_usr_chk():
    uid=request.form['idd']
    qry = "select from_id,msg,date from chatt2 where (from_id='" + str(
        session['oid']) + "' and to_id='" + uid + "') or ((from_id='" + uid + "' and to_id='" + str(
        session['oid']) + "')) order by cid desc"
    c = Db()
    res = c.select(qry)
    return jsonify(res)


@app.route("/arc_chat_usr_post",methods=['POST'])
def arc_chat_usr_post():
    # id=str(session["seluid"])
    ta=request.form["ta"]
    qry="insert into chatt2(msg,date,from_id,to_id,time) values('"+ta+"',CURDATE(),'"+str(session['oid'])+"','"+str(session["toid22"])+"',curtime())"
    d=Db()
    d.insert(qry)
    mm=session["toid22"]
    # return render_template('officer_ci/fur_chat.html',toid=id)
    return redirect(url_for('fff', id=mm))

@app.route('/adm_ofcr_vw22')
def adm_ofcr_vw22():
    oid=session["oid"]
    slc="select officer.*,police_station.name from officer inner join police_station on police_station.pid=officer.pid and officer.oid!='"+str(oid)+"'"
    print(slc)
    res=c.selectall(slc)
    return render_template("officer_ci/view_otr_ofr.html",data=res)



@app.route('/ofr_algo')
def ofr_algo():
    return render_template("officer_ci/objet_idnt.html")

@app.route('/ofr_algo_post',methods=['POST'])
def ofr_algo_post():
    image=request.files["image"]
    image.save(static_path+"mri.jpg")


    import tensorflow as tf
    import sys
    import os

    # Disable tensorflow compilation warnings

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    import tensorflow as tf
    image_data = tf.gfile.FastGFile(static_path + "mri.jpg", 'rb').read()

    # Loads label file, strips off carriage return
    label_lines = [line.rstrip() for line

                   in tf.gfile.GFile(static_project + "logs//output_labels.txt")]

    # Unpersists graph from file
    with tf.gfile.FastGFile(static_project + "logs/output_graph.pb", 'rb') as f:

        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

        _ = tf.import_graph_def(graph_def, name='')

    with tf.Session() as sess:
        # Feed the image_data as input to the graph and get first prediction
        softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')

        predictions = sess.run(softmax_tensor, \
                               {'DecodeJpeg/contents:0': image_data})

        # Sort to show labels of first prediction in order of confidence
        top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
        print("pppppp", len(top_k))
        print(top_k)

        for node_id in top_k:
            human_string = label_lines[node_id]
            score = predictions[0][node_id]
            print('%s (score = %.5f)' % (human_string, score))
            if human_string == "yes":
                print("mridul")
                yes22 = score


            else:
                no22 = score
        print("final result")
        mm=top_k[0]
        if mm==0:
            aa="AK47"
            return render_template("officer_ci/objet_idnt.html",aa=aa)
        elif mm==1:
            aa="AXE"
            return render_template("officer_ci/objet_idnt.html", aa=aa)
        elif mm==2:
            aa="Blood"
            return render_template("officer_ci/objet_idnt.html", aa=aa)
        elif mm==3:
            aa="Glass"
            return render_template("officer_ci/objet_idnt.html", aa=aa)
        elif mm==4:
            aa="Gun"
            return render_template("officer_ci/objet_idnt.html", aa=aa)
        elif mm==5:
            aa="Hammer"
            return render_template("officer_ci/objet_idnt.html", aa=aa)
        elif mm==6:
            aa="Kinfe"
            return render_template("officer_ci/objet_idnt.html", aa=aa)
        elif mm==7:
            aa="Rope"
            return render_template("officer_ci/objet_idnt.html", aa=aa)



if __name__ == '__main__':
    app.run()