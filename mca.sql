/*
SQLyog Community Edition- MySQL GUI v8.03 
MySQL - 5.6.12-log : Database - mca
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`mca` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mca`;

/*Table structure for table `crime` */

DROP TABLE IF EXISTS `crime`;

CREATE TABLE `crime` (
  `cid` int(11) DEFAULT NULL,
  `crime_name` varchar(50) DEFAULT NULL,
  `details` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `crime` */

/*Table structure for table `crime_scene` */

DROP TABLE IF EXISTS `crime_scene`;

CREATE TABLE `crime_scene` (
  `cr_id` int(11) DEFAULT NULL,
  `scene_no` int(11) DEFAULT NULL,
  `scene_details` varchar(50) DEFAULT NULL,
  `scene_date` varchar(50) DEFAULT NULL,
  `scene_time` varchar(50) DEFAULT NULL,
  `scene_file` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `crime_scene` */

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `lid` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`lid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `login` */

insert  into `login`(`lid`,`user_name`,`password`,`type`) values (3,'ram','pwd','si'),(4,'a','a','admin');

/*Table structure for table `object` */

DROP TABLE IF EXISTS `object`;

CREATE TABLE `object` (
  `obid` int(11) NOT NULL AUTO_INCREMENT,
  `crid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`obid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `object` */

/*Table structure for table `officer` */

DROP TABLE IF EXISTS `officer`;

CREATE TABLE `officer` (
  `oid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `qualification` varchar(50) DEFAULT NULL,
  `experience` varchar(50) DEFAULT NULL,
  `house_name` varchar(50) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `photos` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `officer` */

insert  into `officer`(`oid`,`pid`,`name`,`position`,`age`,`qualification`,`experience`,`house_name`,`place`,`pincode`,`email`,`phone`,`photos`) values (16,13,'ram','si',23,'msc','2','hn','plc',6744,'mr@g.com','9847489','static/officers/admn.jpg');

/*Table structure for table `officer_allocation` */

DROP TABLE IF EXISTS `officer_allocation`;

CREATE TABLE `officer_allocation` (
  `of_alloc_id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`of_alloc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `officer_allocation` */

/*Table structure for table `police_station` */

DROP TABLE IF EXISTS `police_station`;

CREATE TABLE `police_station` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `post` varchar(50) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `photos` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `police_station` */

insert  into `police_station`(`pid`,`name`,`place`,`post`,`pincode`,`district`,`email`,`phone`,`photos`) values (13,'atholips3','plc','moda',6733,'kozhikod','a@g.com','9847489','/static/policestation/b2.jpg');

/*Table structure for table `scene_objects` */

DROP TABLE IF EXISTS `scene_objects`;

CREATE TABLE `scene_objects` (
  `cr_id` int(11) DEFAULT NULL,
  `scn_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `details` varchar(100) DEFAULT NULL,
  `st_pos_x` varchar(50) DEFAULT NULL,
  `st_pos_y` varchar(50) DEFAULT NULL,
  `end_pos_x` varchar(50) DEFAULT NULL,
  `end_pos_y` varchar(50) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scene_objects` */

/*Table structure for table `training_set` */

DROP TABLE IF EXISTS `training_set`;

CREATE TABLE `training_set` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `training_set` */

/*Table structure for table `victim` */

DROP TABLE IF EXISTS `victim`;

CREATE TABLE `victim` (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` varchar(20) DEFAULT NULL,
  `house_name` varchar(50) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `blood_group` varchar(20) DEFAULT NULL,
  `id_number` varbinary(20) DEFAULT NULL,
  `photos` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`vid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `victim` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
